﻿using System;
using System.IO;
using JsonEdit;
using Microsoft.Deployment.WindowsInstaller;
using Newtonsoft.Json.Linq;

namespace CustomActionTest
{
    internal class Program
    {
        private const string ProfileGuid = "f06f0de4ce64fe4ae3a751658e940723";

        private const string JavaArgs = "-Xmx5G -XX:+UnlockExperimentalVMOptions -XX:+UseG1GC " +
                                        "-XX:G1NewSizePercent=20 -XX:G1ReservePercent=20 -XX:MaxGCPauseMillis=50 " +
                                        "-XX:G1HeapRegionSize=32M";

        private const string Icon = "Glazed_Terracotta_Light_Blue";
        
        public static void Main(string[] args)
        {
            JsonEditCustomAction();
            JsonRemoveCustomAction();
        }


        private static void JsonEditCustomAction()
        {
            var appdataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var minecraftConfigPath = Path.Combine(appdataDir, @".minecraft\launcher_profiles.json");

            var configJObject = JObject.Parse(File.ReadAllText(minecraftConfigPath));

            var profileProperties = new JObject(
                new JProperty("created", DateTime.Now),
                new JProperty("icon", Icon),
                new JProperty("javaArgs", JavaArgs),
                new JProperty("lastUsed", "1970-01-01T00:00:00.000Z"),
                new JProperty("lastVersionId", "1.16.5-forge-36.0.41"),
                new JProperty("name", "Alchemical Exchange Pack"),
                new JProperty("type", "custom")
            );

            if (configJObject["profiles"] != null)
                configJObject["profiles"][ProfileGuid] = profileProperties;
            File.WriteAllText(minecraftConfigPath, configJObject.ToString());
        }


        private static void JsonRemoveCustomAction()
        {
            var appdataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var minecraftConfigPath = Path.Combine(appdataDir, @".minecraft\launcher_profiles.json");

            var configJObject = JObject.Parse(File.ReadAllText(minecraftConfigPath));

            configJObject["profiles"]?[ProfileGuid]?.Parent?.Remove();
            File.WriteAllText(minecraftConfigPath, configJObject.ToString());
        }
    }
}