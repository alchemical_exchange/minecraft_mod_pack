#tool "nuget:?package=WiX&version=3.11.2"
#addin "nuget:?package=Cake.GitVersioning&version=3.3.37"

var target = Argument("target", "BuildInstaller");

var version = GitVersioningGetVersion().Version.ToString()
				.Substring(0, GitVersioningGetVersion().Version.ToString().LastIndexOf('.'));
				
var msBuildConfig = new MSBuildSettings {
    Configuration = "Release",
    PlatformTarget = PlatformTarget.x86
};

var forge_files_path = System.IO.Path.GetFullPath("../forge/");
var mod_files_path = System.IO.Path.GetFullPath("../mod_files");
var solution_name = @"json_custom_action.sln";
				
Task("BuildCustomAction")
    .Does(() => {
        Information("Cleaning previous builds");
        CleanDirectories("./json_custom_action/bin/");
        CleanDirectories("./json_custom_action/obj/");
        
        Information("Restoring Nuget packages");
        NuGetRestore(solution_name);
        
        Information("Building Minecraft Launcher Config Custom Action");
        MSBuild(solution_name, msBuildConfig);
    });

Task("GetFiles")
	.Does(() => {
        Information("Downloading mod files.");
    });

Task("BuildInstaller")
	.IsDependentOn("GetFiles")
	.IsDependentOn("BuildCustomAction")
	.Does(() => {
        Information($"Building installer version: {version}.");
        WiXHeat(new DirectoryPath(forge_files_path),
            new FilePath("wix.forge_files.wxs"),
            WiXHarvestType.Dir,
            new HeatSettings {
                ArgumentCustomization = args => args.Append("-ke -gg -cg forge_files -sfrag -srd -dr MCINSTALLDIR -var var.forge_files")
            });
        WiXHeat(new DirectoryPath(mod_files_path),
            new FilePath("wix.mod_files.wxs"),
            WiXHarvestType.Dir,
            new HeatSettings {
                ArgumentCustomization = args => args.Append("-ke -gg -cg mod_files -sfrag -srd -dr INSTALLDIR -var var.mod_files")
            });
        WiXCandle(GetFiles("*.wxs"),
            new CandleSettings{
                OutputDirectory = new DirectoryPath("wixobj"),
                Extensions = new List<string> { "WixUIExtension", "WixUtilExtension" },
                ArgumentCustomization = args => args.Append($"-dVersionNumber={version} -dforge_files={forge_files_path} -dmod_files={mod_files_path}"),
                Verbose = true
            });
        WiXLight(GetFiles("wixobj/*.wixobj"),
            new LightSettings {
                OutputFile = new FilePath($"build/AlchemicalExchangeModpack.{version}.msi"),
                Extensions = new List<string> { "WixUIExtension", "WixUtilExtension" },
                ArgumentCustomization = args => args.Append($"-sice:ICE38 -sice:ICE64 -sice:ICE91")
            });
    });

RunTarget(target);
