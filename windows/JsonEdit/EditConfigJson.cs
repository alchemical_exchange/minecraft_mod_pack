using Microsoft.Deployment.WindowsInstaller;
using System;
using System.IO;
using Newtonsoft.Json.Linq;

namespace JsonEdit
{
    public class EditConfigJson
    {
        private const string ProfileGuid = "f06f0de4ce64fe4ae3a751658e940723";

        private const string JavaArgs = "-Xmx4G -XX:+UnlockExperimentalVMOptions -XX:+UseG1GC " +
                                        "-XX:G1NewSizePercent=20 -XX:G1ReservePercent=20 -XX:MaxGCPauseMillis=50 " +
                                        "-XX:G1HeapRegionSize=32M";

        private const string Icon = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAAG0OVFdAAAXm0lEQVR4nO2dB1hTVx/GbybZAZKwVwAFwkaWMkRRCyqCs26ctSq1DvykuGcd9bN2OKtWretDS1FR6wYroOKoGxSQPcKeIWR8ObQgKIGMm1yQ/J7nPoTk3HPe+97/XeeegRWLxZAyYJVaWy0ZeI5d0LqN9rSGC0eOHBklUwaWNvZipsPA1v/RaLTow5U7zaDtyoCUmJ8wHaXrMIO2srviowxEYjFa1pU7zMB73EKhUhlIA2zW/XN7UApnoLQCaSraZRAb/2e4LJmAv43vUiv/fnhfB/vhD22ZPsRpVcT8Lzd3lFFcXNykrd/v3dWaQUcGdUZoaOipUAg6pbyJn/j5IGj0xLRytG7flv/lisTm3YrWbf1f2l7q+HAeM18Mod6nv3f2Z6lHaMcKUKgP/kVJ3VUfZSDPyUS6gg9wD5khTL3wa9enNFvPADHNlPNRIjSeJJsHHa3cAiwnFPsBn4lfJP0p/XzQFWRDq1aTW9Q0Z+AaPFWMI9HaJXaysUwa5mL2y4QJE458mJFQKMTM+3L+OSyB/E8Gjy//Jte5AIPBCPfv2ztW7k3oCKXPB0oLQLR0uASIRGK0lQ1HyLDrD6FxhNbv4/etN2SxWEWwC3BwH1BHMnchtf2O5TSo9fP8MYMiZ04Zv1OWvOQWAI4DSeFSf5f36iazABunfk30Pl6dppe3cJkFOPiPFEsKlzdveAS4Bk0Wk1hmKim8SwFf/2d1DI6sLXNmID4OrP7C28XF5R4sApIzysbJXPq/fLHxQIoVqf72qeO/Duo6tYpORBn1pIBZc+ddPHxw/0iFBUhO0XJHdFuel2NGtL0/GO5q+vO6Vd9EyCTA1t1PTDN3VKb8j7j0OHfhpbELFrb8v3h8wJeTJ07Y3yxgYPCYnAaSgWnLj6BwLBbLnzuyf1Rqyl9e27dvn0Mmk2thVfQvzQISLv8u9TibOW2SKsptLwBJNPcDGgGfhoCIyFWxt5IehIEb9xa8nW2v/rBm0WcqFWDvEyQmG1g2f25bOECWwhUSAK4RknsEEZ6iA7UU/iHy3BnJLcBr3MLmwqWxf9Xc/vLkJ9/TnW+wmKzPlv47rSHe1dU1RSUCTsbGR3RWuJ0ZM+nIrg1dXv8VFvD9b/E/dvb70V0bfOQtXGYBVvauYoatXLsWXgGqKlwmAeC2XJ47Y7gFoGQtfIBfQE7SndtyP0B0KsDOa7CIamIrU0YCPY5p16nkFCBr4S1Iq0pSWIAijAgZlRp/4by70gIEAiFOEQFcvEk/edJLFRD+RcR1SQwqogGaPnPO1WNHfhmmlICUlCR/hp1CJzfodTV+qKxppQog63d8qZUVEJD9Laindu/cNlkhAQRdQ6UEAJLf1UySCJlUk/caenXvpuxV3nADDud/HlTF0L2ze9Bta8A/EjBnUeSfT/PrZQog+UE139Dwq8ugJ9fOvK/sBdi5eDRSrTzwsmQzd3RAdMKV8wG7du0K76oesC25ubkWGzdu3Mn28sxa+92eQ+sjF8xuFgDskRTeLvG4of1/rCl4ix46JDDO39//2kcipk74VtaCWzA1NX134MCBsW2/axagSPUaXHwaDyY9WwDS9QNIg/geQBqNAUgLQJpubUBpeYXhnl+Orow9d3ZKZUW5Nng1iyGQIRQaA7lYG986eezIYGXLQNyAg8f/F/Xt5o3fgmdfFKZjOTgTJ4hl8s9n0LBpbpDr0tmzZ++Go3y1GiC53qCcvfyqUVR9ihZdr/V7pr2fTOuPcjPZuWpldCScmtRigOTxWtzyhEswkf8VTD8O+8bu1V+PwOPxjXBrU6kBh0+dW7Hv7I2t8j7etyXAivLL9o3L58Ioqx0qM+DDRonyEuxitm/96qj5MErqEJUYwBkwTKmNv3xgox6DweDCKEkqsBqQmZPP+XzRuhcUQ2ul8lHXxgNgM2BB5MqLqVkVI1AYheqzEAMWA1z7B1ThjDi0rlN2P5Q2wJrjLNS185GrKXN3QikDfIeGFKhi40Ed0Ya5oWOCgj6LhTvvD1HYAF4jn8SnmSpfeymFNQfjfv9xz97H8ef/cFNVGQCFDbBzcKpjOSn9LNIpXJyRq//oWWWJsYcZqipDYQO0reR6C6EwPDRB13PsfPHd0z9o4XA4Ptz5K2TA+SvXp+HIdLi1dAIK8pn4deONX7dpU6nUKjhzVsiAZUuXHtax84VTh0wEzlhReff0blgjQSEDMGRdxOoRQCSc273S0tTEOAuO/BTaELKB9DYT6mDs15szWz6LmnhQQ/bftUl3bpnRabQKefOS24DcgiIrDJ4o72oqAzRaJ1t7UYbOjCpv+U7YWAc1FbysfHgvWQ+HwzZ1tr5cBjx8+tJ/dMjwBAP34YrqVQsYLTKEYXto+0xc1HyuEIuEEK40LTvpzm2LD9N21noXzXHzqsPpmBAIOgat38O98WQSoToswH0vBSvkPrqf4mFhYfEmODj4dzab/YZAIDSAbiP/6kGJRCIMn8/HV1RUMHJycixfvHjhmpKSMrCuvp7cx4aTTqbr8gor662Tn2eFSNK23qGCSlSBHse8pTWxIO/v0kf37rI+MmDFum0nLt1OmkxkGDf/T7WUublDM8b6jKxAD/sTBIhf9eThAzcGQ5cbFhZ20t7e/oks1Vmzw6dJ/Q28VZaYISASiWCpNzIyyvX29k6YPXv293KJ/IBWA0aMDEl1HhD4ctK4sH2NPJ6WIZOeCQkaBUX5OYb5eXlm48aNO+ru7n5X5mf1mdOV0aU2Wg2Iv3hBvt39iYD4ewGk0RiAtACk0bQPQFoA0mgMQFoA0mgMQFoA0mgMQFoA0mgMQFqANMDz/43EpLCf9u5f/ezJQ2cIg0PjiFQIDWqjJDdvoBV4bm6u0o18u40BqU+eD5w554uLvCYhhaRnBmnRWP/+QoEY9u3bGgTaaO//dsu1L+EoF1ED+E0CLXefgFKRFl2y0eYQ0dwF6qq2MXrGyOlhIcOPw6UBEQNev81yDR4+4hGT4wsRTJ1kXm9SkO/OkOGfnYRTi1oNuHLrr88XRnx1GrQJZDkGyLWuPlMnN2xI/wMtdYRwoRYD4i7fmLls+X8O69p4KdwZ0IdNPclms9NhlqZ6A0ImznhW3ERyABuvKBxaQ3yUBBhltaJSAyqqalj5NWIHLKHrtNIY4221PWr5shXwqWqPSg1wH+BXwrAdoPD6I/o7HIyKXKCyjQeozICA4WNzlNl4Fp2SHzk/fCmMkjpEJQZU19TpVqMopspkvnjS0AWqGkmlLSoxwNVrQJmiXT8BzrpNfwwdOvQ8jJKkAn8X4gnTXyiz8czGvCcHD54fDaOkToHVAIFQiMuvauLgKYrnUVlRLn2oChUAqwF97Bz58t7hIQ1sBpz549L8nrbxANgMWLtp6x6auQNc2akNWAzwDwrN64kbD1DaANARqkqAN9aCQw0CKG0Ax8WTR7XquU0LlDIADDFKNLaTadgBWSnlclldp4IPpQxw8hhQQ7JwhUtLM3SOP4nH4xFBAylYM5aCUgag6YakrlPJB+hyEzR+xpvbF06bwJ13RyhswA8Hft3YtvkcnNTjdY0nTp588/TJk6ptjw8pYcCu77atYjnKNJSpQmTy6IPUcSgobADZQLmucV2CQkGTZi+4G3vicPfrMQJakZJYqj9E83kE1+iVq/Zu2bxJZT1IFTLgdOyFeYqONSUvV58Xf7lJJFoIus2rIn+FDDh48NByiGQMt5YOQWNx0Njxn9+NPRejksH1FDIgKyOdzXJUjwGAfIjlXVNTQ4e7uwxAIQPaDn6gLiIWL/vf0UMHZBq0VB4UNECtd6vNvKrEDqutraVRKJRqOPNVyAA8jQmnBpmJXBF9dN/PP8BaXyi3AZXVNQwUCpmuwo+KBGGgIwScVwS5DbiTdC8YrsIVYVDgkPSEWzdhuwuT24Cr12+prcq6Ixp0ba3cQ+eIG949rk24edWayWAUK5Of3AYkJyf5ow3slClTadBYfHNPseFfrC6qLXgDhQ7x+eXbjesUGmhFbgPKuEVMFsIGtIVi1Ae68bJkTr+QmXP4uU+rU5LuGJNJJJlfqcltAJagxFsPFQL6MhKtvGiDpkXWVOe+hGZPDNu2fOniLtsUyG8AkaqYQjUCZrOKuZu+4ljcqBWLwscsnztrxnfS0spsAGi3N2pi+NP3zde6P0SmCXQw/v6O3YdOb7twYp8d28LioyY2XRpQxC0z6+/jl63Tx705/PG07nkIdAbJwBL9+bLtafzsR5VPUlPavXuUakDM+Svzvvnmm326tt5yt+iSh4Gejn9wTFl/cfOzGa9ePneaMGHCYRcXl/ssFqsYdJRsGYYXRKBAIMDV19eTCwsLTd68eWP36NGj/pmZmX11GMxyI1PzgromtG5aXql3Zn5Jh29p8OZu2m7Dp4m3Lp0RNmxIYBz47iMD/rx9d0LEoiVnwB7XhWl4dywaLQgN9NyvR8ZlpL96ZtPE5+OmTJlywMHB4REOh+u0c3MLwAgwbgCdTgdLha2t7bOQkJD/SUsPnhvS0tLsExISPnvw4IGvBdsqU5tlUCVA4Yj3nmcEb9qwfkdiYmLfVgNALY9lX44QzOQFNl4RgnzcfrPQp/1dUZxPf/L4kTuYOtLPz++6vr5+wftUqp3GpgXw0NSvX79ksHT0++rFXzT/fT+2ssdAYdtpzKThamed6G5rcqWxpgL9ICVpgK+v73XQmsPc3DxLspdUUmujSt73HR426CSarCs5x5ErqCStchIWVclvqME+Sn3g6ejo+DAoKCjW2tr6VbuQnT8PEdFw0mrAzi3rp3ScROUj2iFKt2kujxQaA5AWgDQaA5AWgDQaA5AWgDS9fvyA3k6vPwJ6O5oA6OVoAqCXowmAXo4mAHo5mgDo5WgCoJejCQCYEYpEmMqqamZpWYVBCZdrXFhUbFpdU9M87Qh4ycux6fOkn4vTHSwWK0BaK0ATAF3Q1CTQevYqzePqzYTR9+/fH/gmPc2mtqqCgsZpNTeSAQsY1wlDoEAYfOcjZISHBm75Zf/eRS4//ZSsCYBuxtusHMe1m7fvSUq84Qvav4Bm4KAlNEarbW8wEkQ0d+1yiKcPAQP/hHpa7XK2M7u/cPrBlXDqVpZeGQBF3DLT/6zeeOzOzasBeKouRGKZQy3zhMDd+2t5+Kh5KQnXvOfMnrUL1oxh4pMPANCh+07Kw+GRUdG/lhblM8kGlhBR1wiMs6/SBl8ejn2uQdy35cGDfU+NHxV0QGUFKcknFwCgBV389YQpUd+s3M/jNZAoRn0hcJRDDCuIKVlUTfM8sCO8Io0NWO+CgpaofHYwZfkkAoDXyCe6efuV8wVCAtW4r+R0rg2RzJ0h2Ptyd8G4QM8fMp+mGM0Kn/p9S5PO7k6PD4AzcZfnfxO1Yg8TjNaEUk/31Q/RY9BzrcmNCbMmjNzCXDBDqS4r6qYnBwDKb1hofnk93xCMRYoUkwNdN+mQ8UXh4eE/IyZCCXpkAFRW1zDdPLy5oA03mUpGRIOTtXEivyi99qt5s9bBPa6rOulxAXAsJm7J+rVr/8t08IfUNWJBW9AolMiHTTn+1fwpGy0sLDLULgBmelQABIaMzywqr2Ez7BQfmFQZBjlZnGSSUJnLly9fjYgAFdAjAqC2roHu7O5ZTrd0RZMN1N9Fj0El5mvzC19vil4yUxWz3CJJtw+A28mpo2bNnBHXfKOH0BAFVW/uCS7f/WsIIoWrmG4dABEr1vxx7fqtUKa9P9JSPlm6awCgPPyHlfDQRCbdwhFpLZ803S4AwAQrHGe3epqFM5rYA8Yk6Ol0qwDILyqx9PUbmMGw94XQGBzScnoF3SYAklL/HjZ16tQ/WU4BEBLP972VbhEAcVduhC9bFvkr094PaSm9DsQD4MCxMyt37PzvJrjGJNEgH4gGwJm4S1+Cna9j3Q9JGV1SWlFtmJaW7mhj0/cZ0lrgBrEASE59MjQ6euVeReeXUyfadr74iYs3PLXVEd86dfKEykc0VyeIBEB5ZZX+5MmTrvakaWdwJBqUwRMPCg4J/Tvm1Ak/uIevRQpEAiA4bPxTbbYL1OPu9lEoqAxv7PT57K/uLZ03LTJw8OB4pCUpi9oD4MqNxM8rq+v06GwVTz+gQrgCom3098fOusbEXNy3d+94pPUog9oDYO2GjT+Alrk9HTGORHhYJBw3YuSoR3F//O6FxWJlGvWwu6HWAMgrLLYsLS3VYzD7qLNYlYFCYyCulonrxPA5d3ZsWjtdFZMeqxq1BsB3u3/eSmKZqbNItZDDI3ktWbfj3Iwxn20JCw09hbQeeVBrAMRfiBut3ddbnUWqjYJayGHPqUs7MjMzbZYuWbIOaT2yorYAyM4t6CtG47DgtPmpUtmEMb6YkrawfNUq1qZNmxYirUcW1BYAv52OiSBoq3/eHXVTK0Azk96WjVu3br1g3bq1XyOtpyvUFgDx8fHj8DTVzDvY3ahtQundTSsa/93OnVWRy5atQVpPZ6glAEQiEaYwP9eQ5dRzn/3lpYqPMkx8njvRPCYmY/z48UeR1iMNtQTAq/QM15bu172Jomp+n9hbD+YzGAzu4MGDLyGtpyPUEgDXbiWMBh02eyNvi2u9lu04HF8fuUqkS8IVRkWtiBo1Ivhkd5lgQS0BcPPWreE4ik7XCT9RwIskOtsFLYQg481HLh4HS2MVF2oszxOw6OTcVdHRkcOGDIpFokexei4Bz5856Ngi05unuwKGn5Es2EYIYq/ed/bc6n0xUGNlSXNQGLMYmWvXrFzs7+tzWdU6VB4ADbxGslAkxiLVqaPngIK0tPXBgq2BoL6Ru05cgv57HOJVFkNNFQV8c2OD9A1r10R4evRLgLNUlQfAy9dv3MApUIMCSA4ago4hWPDlEOQQsfXQbbH4INTAzYXQddzq8OlTf1q8KGINBo1WuHeyygMg8W5ycE+Yc7CnAGbwJemZSz6Z087ceR19JnFBdL0kIHAN3PI1a1YvHj0q5Lg8+cEaAKBTx55Dx9fu37tnWZNAgCeyzCCCriH0Kbz+7ba8Dwjdb49ePrbpYOwxQWlmrZu9TeL+PT+OwePxjZ2trnAAgNG3UlKfDIlave5QTka6KXjLR2KZQigMDqJaeyiabbcBDPbk2Jed1MeU9ZhOwpfgUGKegM/DVFdW0urqaikNDQ3E2toaikgoxIpEIpRQJGr1Eo/XaiQSifVkKrUWi8EKsTicEK9F5IsgNE4ghvB8oZhY29Ckw62qNSkqq7Yor6qBrYoUDFaJMeJQXlRAw30nfs3jcbMFBhT063Mxp32oHTRjkzkARGIx+sTZuEVbNm/eJhBBeDD6VnPlDskIYjkawaVfZRjrM7KcbSwTDXXImXiMuK6poR5dWJhvlJXxtg/YWW5ubslgSko2m/2GyWSWEAiEBnVrBDWmPB6PUFNTQwftJgoKCkxzc3Mt3r59y8nJybHEYLECBovF1dbWrcIRSAIRCouraxTqlFTWmmXklzrVNfC022WIQkEEPQtspeT+IXB6ZJWwLKs6wNMl9rvtW2e2PHJ2GgAZ2Xn202bOu15cmGdAM+NA4FmeaoX80W1lZvScY2mcokcn5eLRUB2fV4ctL+Pq5rx7x5YYSLKxsXnu5OSUam1t/VJfX7+QSqVWwTn7vKqQaBSSSKQ6sIApR+3t7Z/ImwcYJq+pqQkvOUORqqqqdMrKyvRKSkoM6urqKJLPLFArWVRUZGxoaJgH0n8UAJLrOCFwxNjMoqICQzrbBULrmEMMHXMYNu9jaBRSeT+O9W0jJu0NVQtbJhY2iWqqKyh52dnmXG6JPovFKnJ2dn4gOTKfGxsb5+jo6JSCI7OnDMGGBMAbcN0HC5ho18zMLLOz9O0CADyzcxxdapsn0aUay1woAY+vd7SxSLYwZLzQJmkVYSAhn99QjystLWZmZ2VZgamPORzOYwcHh8eWlpbpILolR2U1GD1bwe3UABPtAoBI0KrLTH+Jzs7Otnr69Gm/4uLidhd3MplcJzkS34FrpCS6Kmk0WqXkdFXbXUa+1iA/H10CwCnEwsLiLViQEKRBvSDeOVQDsmgCoJejCYBejiYAejmaAOjlaAKgl6MJgF7O/wEhOq/huT4woQAAAABJRU5ErkJggg==";

        /// <summary>
        /// Update launcher config with custom mod pack settings.
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        [CustomAction]
        public static ActionResult UpdateMinecraftConfig(Session session)
        {
            session.Log("Begin launcher config.");
            
            var appdataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var minecraftConfigPath = Path.Combine(appdataDir, @".minecraft\launcher_profiles.json");
            var gameDir = Path.Combine(appdataDir, @"Alchemical Exchange\Minecraft ModPack 1.16.5");

            var configJObject = JObject.Parse(File.ReadAllText(minecraftConfigPath));

            var profileProperties = new JObject(
                new JProperty("created", DateTime.Now),
                new JProperty("gameDir", gameDir),
                new JProperty("icon", Icon),
                new JProperty("javaArgs", JavaArgs),
                new JProperty("lastUsed", "1970-01-01T00:00:00.000Z"),
                new JProperty("lastVersionId", "1.16.5-forge-36.0.43"),
                new JProperty("name", "Alchemical Exchange Pack"),
                new JProperty("type", "custom")
            );

            if (configJObject["profiles"] != null)
                configJObject["profiles"][ProfileGuid] = profileProperties;
            File.WriteAllText(minecraftConfigPath, configJObject.ToString());
            
            return ActionResult.Success;
        }
        
        
        /// <summary>
        /// Remove the custom launcher config.
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        [CustomAction]
        public static ActionResult RemoveMinecraftConfig(Session session)
        {
            session.Log("Begin remove launcher config.");
            
            var appdataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var minecraftConfigPath = Path.Combine(appdataDir, @".minecraft\launcher_profiles.json");

            var configJObject = JObject.Parse(File.ReadAllText(minecraftConfigPath));

            configJObject["profiles"]?[ProfileGuid]?.Parent?.Remove();
            File.WriteAllText(minecraftConfigPath, configJObject.ToString());
            
            return ActionResult.Success;
        }
    }
}
