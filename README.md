# minecraft_mod_pack

# Mod list

## Server
- https://www.curseforge.com/minecraft/mc-mods/easier-sleeping
- https://www.curseforge.com/minecraft/mc-mods/ftb-backups
- https://www.curseforge.com/minecraft/mc-mods/starter-kit

## Client
- https://www.curseforge.com/minecraft/mc-mods/more-overlays-updated

## Common
- https://www.curseforge.com/minecraft/mc-mods/controlling
- https://www.curseforge.com/minecraft/mc-mods/jei/files
- https://www.curseforge.com/minecraft/mc-mods/ftb-gui-library
- https://www.curseforge.com/minecraft/mc-mods/comforts
- https://www.curseforge.com/minecraft/mc-mods/travelers-backpack
- https://www.curseforge.com/minecraft/mc-mods/aquaculture
- https://www.curseforge.com/minecraft/mc-mods/inventory-tweaks-renewed
- https://www.curseforge.com/minecraft/mc-mods/waystones
- https://www.curseforge.com/minecraft/mc-mods/hwyla/files/3033593
- https://www.curseforge.com/minecraft/mc-mods/applied-energistics-2
- https://www.curseforge.com/minecraft/mc-mods/refined-storage
- https://www.curseforge.com/minecraft/mc-mods/pams-harvestcraft-2-food-core
- https://www.curseforge.com/minecraft/mc-mods/pams-harvestcraft-2-crops
- https://www.curseforge.com/minecraft/mc-mods/pams-harvestcraft-2-trees
- https://www.curseforge.com/minecraft/mc-mods/pams-harvestcraft-2-food-extended
- https://www.curseforge.com/minecraft/mc-mods/cooking-for-blockheads
- https://www.curseforge.com/minecraft/mc-mods/ice-and-fire-dragons
- https://www.curseforge.com/minecraft/mc-mods/fast-leaf-decay
- https://www.curseforge.com/minecraft/mc-mods/goblin-traders
- https://www.curseforge.com/minecraft/mc-mods/cat-jammies
- https://www.curseforge.com/minecraft/mc-mods/xaeros-minimap
- https://www.curseforge.com/minecraft/mc-mods/antique-atlas
- https://www.curseforge.com/minecraft/mc-mods/biomes-o-plenty
- https://www.curseforge.com/minecraft/mc-mods/serene-seasons
- https://www.curseforge.com/minecraft/mc-mods/dungeon-crawl
- https://www.curseforge.com/minecraft/mc-mods/dimensional-dungeons
- https://www.curseforge.com/minecraft/mc-mods/woot
- https://www.curseforge.com/minecraft/mc-mods/ping
- https://www.curseforge.com/minecraft/mc-mods/curios
- https://www.curseforge.com/minecraft/mc-mods/the-undergarden
- https://www.curseforge.com/minecraft/mc-mods/spice-of-life-potato-edition
- https://www.curseforge.com/minecraft/mc-mods/building-gadgets
- https://www.curseforge.com/minecraft/mc-mods/charging-gadgets
- https://www.curseforge.com/minecraft/mc-mods/mrcrayfish-furniture-mod
- https://www.curseforge.com/minecraft/mc-mods/ferritecore
- https://www.curseforge.com/minecraft/mc-mods/chisels-bits
- https://www.curseforge.com/minecraft/mc-mods/paragliders
- https://www.curseforge.com/minecraft/mc-mods/botania
- https://www.curseforge.com/minecraft/mc-mods/chargers
- https://www.curseforge.com/minecraft/mc-mods/patchouli
- https://www.curseforge.com/minecraft/mc-mods/storage-drawers
- https://www.curseforge.com/minecraft/mc-mods/iron-chests
- https://www.curseforge.com/minecraft/mc-mods/xnet
- https://www.curseforge.com/minecraft/mc-mods/spawner-fix
- https://www.curseforge.com/minecraft/mc-mods/smooth-boot-forge
- 
- https://www.curseforge.com/minecraft/mc-mods/rftools-base
- https://www.curseforge.com/minecraft/mc-mods/rftools-builder
- https://www.curseforge.com/minecraft/mc-mods/rftools-storage
- https://www.curseforge.com/minecraft/mc-mods/rftools-utility
- https://www.curseforge.com/minecraft/mc-mods/rftools-power
- https://www.curseforge.com/minecraft/mc-mods/rftools-dimensions
- https://www.curseforge.com/minecraft/mc-mods/rftools-control
- 
- https://www.curseforge.com/minecraft/mc-mods/quark
- https://www.curseforge.com/minecraft/mc-mods/autoreglib
- 
- https://www.curseforge.com/minecraft/mc-mods/ars-nouveau
- https://www.curseforge.com/minecraft/mc-mods/geckolib
- 
- https://www.curseforge.com/minecraft/mc-mods/ender-storage-1-8
- https://www.curseforge.com/minecraft/mc-mods/codechicken-lib-1-8
- https://www.curseforge.com/minecraft/mc-mods/chicken-chunks-1-8
- 
- https://www.curseforge.com/minecraft/mc-mods/immersive-engineering
- https://www.curseforge.com/minecraft/mc-mods/engineers-decor
- 
- https://www.curseforge.com/minecraft/mc-mods/rats
- https://www.curseforge.com/minecraft/mc-mods/rats-ratlantis
- https://www.curseforge.com/minecraft/mc-mods/citadel
- 
- https://www.curseforge.com/minecraft/mc-mods/minecolonies
- https://www.curseforge.com/minecraft/mc-mods/structurize/files/3175872
- 
- https://www.curseforge.com/minecraft/mc-mods/evilcraft
- https://www.curseforge.com/minecraft/mc-mods/cyclops-core
- 
- https://www.curseforge.com/minecraft/mc-mods/techemistry
- https://www.curseforge.com/minecraft/mc-mods/alchemistry
- https://www.curseforge.com/minecraft/mc-mods/a-lib
- https://www.curseforge.com/minecraft/mc-mods/chemlib
- 
- https://www.curseforge.com/minecraft/mc-mods/create
- https://www.curseforge.com/minecraft/mc-mods/createaddition
- 
- https://www.curseforge.com/minecraft/mc-mods/electrodynamics
- https://www.curseforge.com/minecraft/mc-mods/assembly-lines
- https://www.curseforge.com/minecraft/mc-mods/nuclear-science
- 
- https://www.curseforge.com/minecraft/mc-mods/magic-feather
- https://www.curseforge.com/minecraft/mc-mods/cookiecore

# Command Reference
- `/spreadplayers ~ ~ 1000 20000 true @a[distance=..5]`
  - ~ ~: Centered at block's location
  - 1000: Distance players should be apart from each other
  - 20000: Range player may teleport to
  - true: Keep players on the same team together
  - @a[distance=..5]:Include all players within 5 blocks of the command block

- `/give @p travelersbackpack:standard{CraftingInventory:{Size:9,Items:[]},RightTank:{FluidName:"minecraft:water",Amount:4000},LeftTank:{FluidName:"minecraft:water",Amount:4000},Inventory:{Size:45,Items:[{Slot:0,id:"mysticalworld:iron_knife",Count:1b,tag:{Damage:0}},{Slot:1,id:"minecraft:bow",Count:1b,tag:{Damage:0}},{Slot:2,id:"minecraft:arrow",Count:32b},{Slot:3,id:"minecraft:fishing_rod",Count:1b,tag:{Damage:0}},{Slot:4,id:"minecraft:carrot",Count:32b},{Slot:5,id:"minecraft:potato",Count:32b},{Slot:6,id:"minecraft:wheat_seeds",Count:24b},{Slot:7,id:"minecraft:bucket",Count:1b},{Slot:8,id:"antiqueatlas:empty_antique_atlas",Count:1b},{Slot:9,id:"minecraft:oak_boat",Count:1b},{Slot:10,id:"minecraft:oak_log",Count:64b},{Slot:11,id:"minecraft:oak_sapling",Count:8b},{Slot:12,id:"waystones:waystone",Count:1b},{Slot:13,id:"minecraft:campfire",Count:1b},{Slot:14,id:"minecraft:white_bed",Count:1b},{Slot:39,id:"minecraft:iron_axe",Count:1b,tag:{Damage:0}},{Slot:40,id:"minecraft:iron_pickaxe",Count:1b,tag:{Damage:0}}]}}`
